/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.algorithm2;

import java.util.Scanner;

/**
 *
 * @author hanam
 */
public class StringToInteger {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String S = kb.nextLine();
        System.out.println(stringToInteger(S));
    }
       public static int stringToInteger(String S){
           int Result = 0;           //ตัวเก็บ ผลรวม ทั้งหมดของจำนวนเต็ม
           int ValueOfDigit = 1; //ตัวระบุหลักของหน่วยตัวเลขโดยเริ่มจาก หลักหน่วย สิบ พัน หมื่น 
           for(int i =S.length()-1;i>=0;i--){ // i เริ่มจากตัวสุดท้ายของประโยค S จนถึง ตัวหน้าสุดของประโยค S
            char noun = S.charAt(i);
            switch(noun){ //ตรวจหาว่า noun ตำแหน่งที่ i เป็นตัวอะไร?
                case('0'):
                    Result += (ValueOfDigit*0); 
                    break;
                case('1'):
                    Result += (ValueOfDigit*1);
                    break;
                case('2'):
                    Result += (ValueOfDigit*2);
                    break;
                case('3'):
                    Result += (ValueOfDigit*3);
                    break;
                case('4'):
                    Result += (ValueOfDigit*4);
                    break;
                case('5'):
                    Result += (ValueOfDigit*5);
                    break;
                case('6'):
                    Result += (ValueOfDigit*6);
                    break;
                case('7'):
                    Result += (ValueOfDigit*7);
                    break;
                case('8'):
                    Result += (ValueOfDigit*8);
                    break;
                case ('9'):
                    Result += (ValueOfDigit * 9);
                    break;
                case ('-'):          //เช็คในส่วนเมื่อ noun เป็นเครื่องหมาย '-' และต้องเป็นตำแหน่งหน้าสุด 
                    if (i == 0) {    // แล้วนำ Result ไปคูณกับ -1  เพื่อเทำให้เป็นจำนวนเต็มลบ
                        Result *= -1;
                    }
                    break;  
                default:          //ปล่อยว่าง
            }
            ValueOfDigit*=10; //ขยับไปที่หลักต่อไป
        }
           return Result; //ส่งคำตอบที่เป็น ผลบวกของแต่ละหลัก กลับไป
       }
}
